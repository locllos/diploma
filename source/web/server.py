from common.serializer import SimpleSerializer

import asyncio
import logging

from dataclasses import dataclass
from random import sample

from typing import List
from typing import Set

logging.basicConfig(level=logging.INFO, filename="server.log",filemode="w")

class Server:
  @dataclass
  class Client:
    reader: asyncio.StreamReader
    writer: asyncio.StreamWriter

  def __init__(
    self,
    address: str = '127.0.0.1',
    port: int = 8888,
    serializer: SimpleSerializer = SimpleSerializer()
  ):  
    self.address: str = address
    self.port: int = port
    self.is_closed: bool = False
    self.clients: List[Server.Client] = []
    self.serializer = serializer
    self.server = None

  def run(self):
    async def start():
      logging.info("Server is starting...")
      self.server = await asyncio.start_server(
        lambda reader, writer: self.handle_new_connection(reader, writer),
        host=self.address,
        port=self.port
      )
      logging.info("Server successfully started")
    
    async def loop():
      async with self.server:
        logging.info("Loop successfully started")
        await self.server.serve_forever()
    
    asyncio.run(start())

  async def handle_new_connection(
    self,
    reader: asyncio.StreamReader,
    writer: asyncio.StreamWriter
  ):
    peername = writer.get_extra_info('peername')
    logging.info(f"New connection: {peername}")

    while reader.at_eof() is False:
      await self.handle_connection(client=Server.Client(reader, writer))

    logging.info(f"Connection: {peername} was closed")
    writer.close()
    await writer.wait_closed()


  async def handle_connection(self, client: Client):
    peername = client.writer.get_extra_info('peername')

    try:
      logging.info(f"Awaiting read from {peername}")
      raw = await client.reader.readuntil(b"$")
    except:
      logging.warning(
        f"{peername} closed the connection while reading."
      )

    data = raw.decode("utf-8")
    logging.info(f"Data {data} by {peername}")

    try:
      client.writer.write(f"{data}".encode())
      await client.writer.drain()
    except:
      logging.warning(
        f"{peername} closed the connection while writing."
      )


server = Server()
server.run()


# Что хочу иметь

class TCPServer:
  @dataclass
  class Client:
    reader: asyncio.StreamReader
    writer: asyncio.StreamWriter

  def __init__(
    self,
    address: str = '127.0.0.1',
    port: int = 8888,
    clients_count: int = 0,
    timeout: float = 0,
    serializer: SimpleSerializer = SimpleSerializer()
  ):
    self.clients_count = clients_count
    self.clients: dict[TCPServer.Client] = []
    self.address: str = address
    self.port: int = port
    self.timeout = timeout
    self.serializer = serializer
    self.server = None
    self.core = None

  async def start(self):
    async def start():
      logging.info("Server is starting...")
      self.server = await asyncio.start_server(
        lambda reader, writer: self.handle_new_connection(reader, writer),
        host=self.address,
        port=self.port
      )
      logging.info("Server successfully started")

    async def loop():
      async with self.server:
        logging.info("Loop successfully started")
        await self.server.serve_forever()
    
    asyncio.run(start())

    self.worker = asyncio.create_task(loop())

  def is_started(self):
    return self.worker is not None
  
  def stop(self):
    if self.is_started is False:
      return False
    return self.worker.cancel("Server was stopped")

  async def send(self, message: str, receivers: List[str] | int):
    """
      empty receivers means that we want to send message to all clients
    """
    target: List[str] = []
    if type(receivers) is List and receivers is [] or \
       type(receivers) is int and receivers == 0:
      target = list(self.clients.keys())
    elif type(receivers) is List:
      target = receivers
    elif type(receivers) is int:
      target = sample(list(self.clients.keys()), receivers)
    else:
      raise ValueError
    
    tasks = []
    for writer, peername in [(self.clients.get(peername).writer, peername) for peername in target]:
      writer.write(message)

      async def timeout_drain():
        try:
          await asyncio.wait_for(writer.drain(), self.timeout)
          return None
        except: # clarify exception?
          return peername
        
      tasks.append(asyncio.create_task(timeout_drain()))
    
    asyncio.gather(*tasks)

  async def receive(self, clients_count):
    pass 

  async def _handle_new_connection(
    self,
    reader: asyncio.StreamReader,
    writer: asyncio.StreamWriter
  ):
    peername = writer.get_extra_info('peername')
    self.clients[peername] = TCPServer.Client(reader, writer)
    
    logging.info(f"New connection: {peername}")


class TCPClient:
  def __init__(self, address: str, port: int):
    pass

  def Launch():
    pass

  async def launch():
    pass

  async def send(message):
    pass

  async def receive():
    pass