import pickle

class SimpleSerializer:
  def serialize(instance):
    return pickle.dumps(instance)

  def deserialize(raw):
    return pickle.loads(raw)
