from web.server import TCPServer

server = TCPServer()
print(f"{server.is_started()=}")
server.start()

while (got_input := input(">")) != "$":
  server.send(got_input.encode("utf-8"))

server.stop()
print(f"{server.is_started()=}")